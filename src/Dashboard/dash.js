import React, { Component } from 'react';
import {Redirect} from 'react-router-dom'
import './dash.css'
import './Chat/PublicChat'
import PublicChat from './Chat/PublicChat';
import GroupeChat from './Chat/GroupeChat';
import PrivateChat from './Chat/PrivateChat';
import Li from "./DashFunctions/Li"

import SockJS from 'sockjs-client'
import SockJsClient from 'react-stomp';


class dash extends Component {
    constructor(props){
      super(props);

                  this.state={
                    redirect: false,
                    error: undefined,
                    isLoaded: false,
                    connectedUser: [],
                    usersareLoaded: false,
                    users: [],
                    groups: [],
                   
                    clientRef : "",
                    idgroup:"",
                    username:"",
                    pMessages: [],
                    gMessages: [],
                    prMessages: [],
                    // aPI
                    publicMessages:  [],
                    privateMessages: [],
                    groupeMessages: [],                    

                  }
                 

    this.logout = this.logout.bind(this)
    this.getData = this.getData.bind(this);
    this.getAllUsers =this.getAllUsers.bind(this);
    this.getGroupeChat =this.getGroupeChat.bind(this);
    this.getPublicChat =this.getPublicChat.bind(this);


    }

    componentWillMount(){
      this.getData();
      this.getAllUsers ();
      this.getAllGroups();
      this.getPublicChat();


    }
    
  getData () {
      const myHeaders = new Headers({
        'Content-Type': 'application/json',
        'Authorization': "Bearer  "+ localStorage.getItem('token')
    });
      return fetch("http://192.168.1.2:5000/api/me", {
        method: 'GET',
        headers:myHeaders
      })
          .then(res => res.json())
          .then(
            (result) => {
              console.log(result)
              this.setState({
                isLoaded: true,
                connectedUser: result
              });
            },
            (error) => {
              /*this.setState({
                isLoaded: true,
                error
              });*/
              console.log("Error :" + error)
            }
          )
        }
       
        getAllGroups () {
          const myHeaders = new Headers({
            'Content-Type': 'application/json',
            'Authorization': "Bearer  "+ localStorage.getItem('token')
        });
          return fetch("http://192.168.1.2:5000/api/groupes", {
            method: 'GET',
            headers:myHeaders
          })
              .then(res => res.json())
              .then(
                (result) => {
                  console.log(result)
                  this.setState({
                    groupsareLoaded: true,
                    groups: result
                  });
                },
                (error) => {
                  /*this.setState({
                    isLoaded: true,
                    error
                  });*/
                  console.log("Error :" + error)
                }
              )
            }    
        getAllUsers () {

          const myHeaders = new Headers({
            'Content-Type': 'application/json',
            'Authorization': "Bearer  "+ localStorage.getItem('token')
        });
          return fetch("http://192.168.1.2:5000/api/users", {
            method: 'GET',
            headers:myHeaders
          })
              .then(res => res.json())
              .then(
                (result) => {
                  console.log(result)
                  this.setState({
                    usersareLoaded: true,
                    users: result
                  });
                },
                (error) => {
                  /*this.setState({
                    isLoaded: true,
                    error
                  });*/
                  console.log("Error :" + error)
                }
              )
            }    

              getPublicChat () {
                
                      const myHeaders = new Headers({
                        'Content-Type': 'application/json',
                        'Authorization': "Bearer  "+ localStorage.getItem('token')
                    });
                      return fetch("http://192.168.1.2:5000/api/messages/public", {
                        method: 'GET',
                        headers:myHeaders
                      })
                          .then(res => res.json())
                          .then(
                            (result) => {
                              console.log(result)
                              this.setState({
                                pMessagesareLoaded: true,
                                publicMessages: result
                              });
                              
                            },
                            (error) => {
                              /*this.setState({
                                isLoaded: true,
                                error
                              });*/
                              console.log("Error :" + error)
                            }
                          )
                        }    
            getGroupeChat (id) {
              this.setState({
                gMessages :[]
               })
              const myHeaders = new Headers({
                'Content-Type': 'application/json',
                'Authorization': "Bearer  "+ localStorage.getItem('token')
            });
              return fetch("http://192.168.1.2:5000/api/messages/groupe?id="+id, {
                method: 'GET',
                headers:myHeaders
              })
                  .then(res => res.json())
                  .then(
                    (result) => {
                      console.log(result)
                      this.setState({
                        groupeMessages: result,
                        idgroup:id

                      });
                      
                    },
                    (error) => {
                      /*this.setState({
                        isLoaded: true,
                        error
                      });*/
                      console.log("Error :" + error)
                    }
                  )
                }   

                getPrivateChat(username) {
                   this.setState({
                    prMessages :[]
                   })


                  const myHeaders = new Headers({
                    'Content-Type': 'application/json',
                    'Authorization': "Bearer  "+ localStorage.getItem('token')
                });
                  return fetch("http://192.168.1.2:5000/api/messages/private?username="+username, {
                    method: 'GET',
                    headers:myHeaders
                  })
                      .then(res => res.json())
                      .then(
                        (result) => {
                          console.log(result)
                          this.setState({
                            privateMessages: result,
                            username: username
                          });
                          
                        },
                        (error) => {
                          /*this.setState({
                            isLoaded: true,
                            error
                          });*/
                          console.log("Error :" + error)
                        }
                      )
                    }    
          


            logout(){
              localStorage.setItem("token",'undefined')
              localStorage.clear();
              this.setState({ redirect: true })
            }
           /* groupClickToChat(id){
              
              this.setState({
                idgroup:id
              })
              this.getGroupeChat (id)
                console.log(id)
            }*/
   
    render() {
      var  connectedUser = this.state.connectedUser
      var users = this.state.users
      var groups = this.state.groups
      var isLoaded = this.state.isLoaded
      var usersareLoaded = this.state.usersareLoaded
      var groupsareLoaded = this.state.groupsareLoaded
      var url ="https://chatapplicationsaaden.herokuapp.com"
      if(!isLoaded && !usersareLoaded){
        return <div>Loading...</div>
      }

      if(this.state.redirect){
        return (<Redirect to={'/'} />);
      }else{
    return (

      <div className="dash">
        <h1>Congrats You are Logged In  </h1>        
            <div>
                  <img class="contact-pic" src={require('AAA/assets/pages/media/users/avatar4.jpg')} ></img>
                  <span class="contact-name"> {connectedUser.username}</span>
                  <span class="contact-status bg-green"></span>
            </div>


      <button type="button" className="btn red pull-right" onClick={this.logout}>Logout</button>
      
      <PublicChat {...this.state} />
      <GroupeChat {...this.state} />
      <PrivateChat {...this.state} />
      
        <div className="divusers">
        <div className="mt-element-list">
                      <div className="mt-list-head list-simple ext-1 font-white bg-green-sharp">
                            <div className="list-head-title-container">
                                 <h3 className="list-title">Liste des Utilisateurs </h3>

                            </div>
                      </div>

                      {users.map(user =>(
                      <div className="mt-list-container list-simple ext-1">
                        <ul>
                            <li className="mt-list-item done" key={user.username}>
                                <div className="list-icon-container">
                                      <i className="icon-check"></i>
                                        </div>
                                           <div className="list-item-content">
                                              <h3 className="uppercase">
                                              <button onClick={this.getPrivateChat.bind(this, user.username)} > {user.username}</button>
                                              </h3>
                                                  </div>
                            </li>
                          </ul>
                      </div>
                                                ))};
          </div>
          </div>        
          <div className="divusers">
        <div className="mt-element-list">
                      <div className="mt-list-head list-simple ext-1 font-white bg-green-sharp">
                            <div className="list-head-title-container">
                                 <h3 className="list-title">Liste des Groupes</h3>
                            </div>
                      </div>
                       {connectedUser.groupes.map(groupe =>(
                      <div className="mt-list-container list-simple ext-1">
                        <ul>
                            <li className="mt-list-item done" key={groupe.id}>
                                <div className="list-icon-container">
                                      <i className="icon-check"></i>
                                        </div>
                                           <div className="list-item-content">
                                              <h3 className="uppercase">
                                                <button onClick={this.getGroupeChat.bind(this, groupe.id)} > {groupe.nom}</button>
                                              </h3>
                                                  </div>
                            </li>
                          </ul>
                      </div>
                                              ))};
          </div>
          </div>
          
              <SockJsClient url='http://192.168.1.2:5000/websocket' topics={['/topic/messages']}
                 
                onMessage={(msg) => { //console.log(msg)
                  //console.log(this.state.messages)
                  var ar ;
                  
                  switch(msg.type){
                    case "groupe" : ar = this.state.gMessages;
                    break;
                    case "public" : ar = this.state.pMessages;
                    break;
                    case "private" : ar = this.state.prMessages;
                    break;
                  }
                  if( (msg.type=="groupe" && msg.toGroupe==this.state.idgroup )||
                  (msg.type=="private" && 
                 ( (msg.toUsername==this.state.username && msg.from == connectedUser.username)
                || (msg.toUsername==connectedUser.username && msg.from == this.state.username  )) ||
                  (msg.type=="public" )
                    )){

                  ar.push(msg)  
                  this.setState({ messages: ar });
                    
                }
                 // console.log(this.state.messages)
                  /*
                     //mess = msg.content ;
                  var li = document.createElement("li")
                  li.className = "list-group-item"
                  var t = document.createTextNode(msg.from + " " + msg.texte)
                  li.appendChild(t)
                  {document.getElementById(msg.type).appendChild(li)} 
                 } */

                }}
            ref={ (client) => { this.clientRef = client 
        
            }}
            
            onConnect={() =>
            {
              this.setState({
                clientRef : this.clientRef
              })
              console.log("Connected");
            }
            }
                
            />
    </div>
    );
  }
  }
}

export default dash;
                                                                                                                                                                                                                                                                                                                                                                                      