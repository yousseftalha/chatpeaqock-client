import React, { Component } from 'react'
import PropTypes from 'prop-types'
import 'bootstrap/dist/css/bootstrap.min.css'
import SockJsClient from 'react-stomp';
import Li from "../DashFunctions/Li"


class PrivateChat extends Component {
      constructor(props){
        super(props);
        this.state={
          publicChatInput:"",
          messages : this.props.messages,
          amessage:"",
          allMessages :[]
          
        }

        this.handleformsubmi = this.handleformsubmi.bind(this);
      }
     


      handleformsubmi(e){
        e.preventDefault();
        let cUser = this.props.connectedUser.username
        let chatInput = this.refs.messageText.value;
        this.setState({publicChatInput: this.refs.messageText.value});
        //this.setState({ messages: [...this.state.messages, chatInput] });
         let data = {from:cUser,type:"private",toUsername:this.props.username,texte:chatInput} 
       // let data = {from:"saad",toGroupe:2,type:"Groupe",texte:"saaaaaaad this is a groupe message"}

        this.props.clientRef.sendMessage('/chat', JSON.stringify(data));

       
        //this.props.actions.send(data);
        //console.log(this.state.publicChatInput)
      }


  render() {
    // messages = this.state.publicChatInput;
    var returnedMessage = this.state.returnedMessage
    var privateMessages = this.state.privateMessages
    var amessage = this.state.amessage
    var message = this.props.messages
    var mess =""
     let i=0  

    // console.log(this.props.messages)
    
    
    return (
      <div>
          <div className="container">  
            <form onSubmit={this.handleformsubmi}>
                  <div className="form-group">
                    <div className="input-group">
                        <input type="text" ref="messageText" className="form-control"/>
                        <span/>
                        <button type="submit" className="btn btn-primary">send </button>
                    </div>
                  </div>
            </form>`
            
              
              <ul className="list-group" id="private">
              <div >
                {this.props.privateMessages.map( pMessage => (
                  <li className="list-group-item" > {pMessage.userFrom.username} : {pMessage.message} : {pMessage.date}
                    </li>
               ))}
               <Li messages={this.props.prMessages} />
              </div>
                               
              </ul>

          </div> 
        </div>
    )
  }
}

PrivateChat.propTypes = {
 
}

export default PrivateChat