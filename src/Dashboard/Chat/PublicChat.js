import React, { Component } from 'react'
import PropTypes from 'prop-types'
import 'bootstrap/dist/css/bootstrap.min.css'
import SockJsClient from 'react-stomp';
import Li from "../DashFunctions/Li"


class PublicChat extends Component {
      constructor(props){
        super(props);
        this.state={
          publicChatInput:"",
          messages : [],
          publicMessages: [] ,
          amessage:""
          
        }
        this.handleformsubmi = this.handleformsubmi.bind(this);

      }
    
      handleformsubmi(e){
        e.preventDefault();
        let cUser = this.props.connectedUser.username
        let publicChatInput = this.refs.messageText.value;
        this.setState({publicChatInput: this.refs.messageText.value});
        this.setState({ messages: [...this.state.messages, publicChatInput] });
         let data = {from:cUser,type:"Public",texte:publicChatInput} 
       // let data = {from:"saad",toGroupe:2,type:"Groupe",texte:"saaaaaaad this is a groupe message"}

       
       this.props.clientRef.sendMessage('/chat', JSON.stringify(data));

        //this.props.actions.send(data);
        //console.log(this.state.publicChatInput)

      }


  render() {
    // messages = this.state.publicChatInput;
    var returnedMessage = this.state.returnedMessage
    var publicMessages = this.state.publicMessages
    var amessage = this.state.amessage
    var mess =""
     let i=0  
    
    
    return (
      <div>
          <div className="container">  
            <form onSubmit={this.handleformsubmi}>
                  <div className="form-group">
                    <div className="input-group">
                        <input type="text" ref="messageText" className="form-control"/>
                        <span/>
                        <button type="submit" className="btn btn-primary">send </button>
                    </div>
                  </div>
            </form>`
            
              
              <ul className="List-group" id="public">
                {this.props.publicMessages.map( pMessage => (
                  <li className="list-group-item" > {pMessage.userFrom.username} : {pMessage.message}
                    </li>
                   
               
               ))}
              
              <Li messages={this.props.pMessages} />
           
              </ul>

          </div> 

      
      {/* <SockJsClient url='http://192.168.1.144:5000/websocket' topics={['/topic/messages']}
                onMessage={(msg) => { console.log(msg)  

                  //mess = msg.content ;
                  var li = document.createElement("li")
                  var t = document.createTextNode(msg.from + " " + msg.texte)
                  li.appendChild(t)
                  {document.getElementById(msg.type).appendChild(li)}

                }}
            ref={ (client) => { this.clientRef = client }}
                
            /> */}
      
        </div>
    )
  }
}

PublicChat.propTypes = {
 
}

export default PublicChat