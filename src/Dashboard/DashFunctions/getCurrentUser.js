import React from 'react'

export default function getCurrentUser() {

        console.log(localStorage.getItem('token'))
        const myHeaders = new Headers({
          'Content-Type': 'application/json',
          'Authorization': "Bearer  "+ localStorage.getItem('token')
      });
        return fetch("http://192.168.1.144:5000/api/me", {
          method: 'GET',
          headers:myHeaders
        })
            .then(res => res.json())
            .then(
              (result) => {
                console.log(result)
                this.setState({
                  isLoaded: true,
                  connectedUser: result
                });
              },
              (error) => {
                /*this.setState({
                  isLoaded: true,
                  error
                });*/
                console.log("Error :" + error)
              }
            )
    }
 

