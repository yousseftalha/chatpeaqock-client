import React, { Component } from 'react';
import {BrowserRouter as Router,Switch ,Route,Redirect} from 'react-router-dom';
import dash from './Dashboard/dash';


import './App.css';


import 'AAA/assets/global/plugins/font-awesome/css/font-awesome.min.css';
import 'AAA/assets/global/plugins/simple-line-icons/simple-line-icons.min.css';
import 'AAA/assets/global/plugins/bootstrap/css/bootstrap.min.css';
import 'AAA/assets/global/plugins/uniform/css/uniform.default.css';
import 'AAA/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css';
import 'AAA/assets/global/plugins/select2/css/select2.min.css';
import 'AAA/assets/global/plugins/select2/css/select2-bootstrap.min.css';
import 'AAA//assets/global/css/components-md.min.css';
import 'AAA/assets/global/css/plugins-md.min.css';
import 'AAA/assets/pages/css/login-4.min.css';


import Login from './Login/Login';
import RequireAuth from './Login/RequireAuth'
import SignUp from './SignUp/SignUp';


const checkAuth = () => {
  //const token = localStorage.getItem('token');
  //const tokentype = localStorage.getItem('tokentype');
  if (localStorage.getItem("token") == 'undefined' || !localStorage.getItem("token")) {
      return false
  }
      return true;  
      }
const AuthRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={(props) => (
         checkAuth()
     ? <Component {...props}/>
      :<Redirect to={{
        pathname: '/',
        state:  {from: props.location}
      }} />
    
    )}/>
)

class App extends Component {
  render() {
    return (
      <Router>
      <div className="App" >
      <Route path='/SignUp' component={SignUp} />
      <Route path='/' component={Login} />  
      <Switch>
      <AuthRoute path='/dash' component={dash}  /> 
      </Switch>
       </div>
       </Router>
    );
  }
}

 

export default App;
