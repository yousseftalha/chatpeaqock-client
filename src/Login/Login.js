import React, { Component } from 'react';
import test from './Post';

import './Login.css';
import '../App.css';
import {Router ,Redirect} from 'react-router-dom';
import dash from '../Dashboard/dash';
class Login extends Component {
    constructor(props){ 
        super(props);
        this.state={
            username:"",
            password:"",
            redirect:false
        }
        this.connect=this.connect.bind(this);
        this.onChange=this.onChange.bind(this);
        }
     session = {
        tokentype: localStorage.getItem('tokentype'),
        token: localStorage.getItem('token')

    }

    /* headers = () => {
        const h = new Headers();

        h.append('Content-Type' , 'application/json');

        const session = {

           // tokentype:localStorage.getItem('tokentype'), 
            //token:localStorage.getItem('token')

        };
        if(session.username && session.token){

            h.append('username',session.user);
            h.append('token',session.token);
        }

        return h;

    }*/
        


    connect(){

        test("login", this.state).then((result) =>{
            let responseJSON = result;
            console.log(responseJSON.accessToken)
            if(responseJSON.accessToken){
                //this.setState({redi:true});
                //sessionStorage.setItem('accessToken' , responseJSON)
                this.setState({ redirect: true })
            }else{
                alert("Login error");
            }

        });
   
    }
    onChange(e){
        this.setState({[e.target.name]: e.target.value});
        //console.log(this.state);
    }
    SignUp(){
        this.setState({
            
        })
    }    

    render() {
        if(this.state.redirect){

                 return (<Redirect to={'/dash'} />);
        }
        

        if (this.state.redirect && localStorage.getItem("token") != 'undefined' && localStorage.getItem("token")) {
            return (<Redirect to={'/dash'} />);
        }

    return (
        <div className="backstretch"  src="AAA/assets/pages/media/bg/4.jpg">
        <div className="App">
        <div className="login" >
        <div className="logo">
        
            <a href="index.html">
                <img src="http://peaqock.cluster014.ovh.net/wp-content/uploads/2018/01/logo_peaqock-300x76.png" alt="Peaqcock" /> </a>
        </div>
        <div className="content" >
            <div className="login-form" >
                <h3 className="form-title">Login to your account</h3>
                <div className="alert alert-danger display-hide">
                    <button className="close" data-close="alert"></button>
                    <span> Enter any username and password. </span>
                </div>
                <div className="form-group">
                    <label className="control-label visible-ie8 visible-ie9">Username</label>
                    <div className="input-icon">
                        <i className="fa fa-user"></i>
                        <input className="form-control placeholder-no-fix" onChange={this.onChange} type="text" autoComplete="off" placeholder="Username" name="username" /> </div>
                </div>
                <div className="form-group">
                    <label className="control-label visible-ie8 visible-ie9">Password</label>
                    <div className="input-icon">
                        <i className="fa fa-lock"></i>
                        <input className="form-control placeholder-no-fix" onChange={this.onChange}  type="password" autoComplete="off" placeholder="Password" name="password" /> </div>
                </div>
                
                    
                    <input type="submit" text="Login" className="btn green pull-right" onClick={this.connect} /> 
                          
                </div>
                </div>
            </div>
            </div>
            <input type="submit" text="Sign Up" className="btn green pull-left" onClick={this.SignUp} />
            </div> 
    );
    
  }
}

export default Login;
