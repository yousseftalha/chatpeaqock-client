 
 export default function est(type , userData){

    let BaseUrl = "https://api.thewallscript.com/restful/login";

    return new Promise((resolve , reject) => {

        return fetch(BaseUrl + Type,{
            method: "POST",
            body: JSON.stringify(userData)
        })
        .then((response) => response.json())
        .then((responseJson) => {
            resolve(responseJson);
        })
        .catch((error) => {
          reject(error);
        });

    });

}

