const config = {
    headers: {
      accept: 'application/json',
    },
    data: {},
  };

export default function  test(Type , userData){
console.log(userData);

    let BaseUrl = "http://192.168.1.2:5000/api/auth/Login";

    return new Promise((resolve , reject) => {

        return fetch(BaseUrl ,{
            method: "POST",
             headers: {"Content-Type": "application/json"},
             body: JSON.stringify(userData)
        })
        .then((response) => response.json())
        .then((responseJson) => {
            resolve(responseJson);
            
          let token = responseJson.accessToken;
          let tokentype = responseJson.tokenType;
          
          if(token){
            //this.setState({redi:true});
            localStorage.setItem('tokentype', tokentype);
            localStorage.setItem('token', token);
          }else{
            console.log("Login error");
          }
         
        })
        .catch((error) => {
          reject(error);
        });

    });

}
